// TODO доработать до 0.5
import modules from '../src/config/widgets.json';

const child_process = require('child_process');

function updateScripts() {
    const packageJson = require(`${__dirname}/../package.json`);
    const version = packageJson['protopia-ecosystem'].scripts;
    child_process.execSync(`cd ${__dirname} && git fetch && git checkout ${version}`);
}

updateScripts();
